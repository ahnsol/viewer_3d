import axios from 'axios';
import {API_KEY, BASE_URL} from '../config';
import Constant from '../Constant';

export default {
  [Constant.FETCH_PRODUCTS] : async (store) => {
    await axios.get(`${BASE_URL}/v3/assets`, {
      headers: {
        'x-api-key': API_KEY, 'Content-Type': 'application/json',
      },
      params : {
        'link': 10
      }
    }).then(res => {
      const productList = [...res.data.data.assets];
      store.commit(Constant.FETCH_PRODUCTS, {productList});
    }).catch(err => console.log(err));
  },
  [Constant.FETCH_WALL]     : async (store) => {
    await axios.get(`${BASE_URL}/v3/materials`, {
      headers  : {
        'x-api-key': API_KEY
      }, params: {
        'include': {
          'material_products': {
            'join' : true,
            'where': {
              'm_plane_type_id': '3' // master code에 명시된 벽지의 plane_type_id
            }
          }
        },
        'link': 10
      }
    }).then(res => {
      const wallList = [...res.data.data.materials];
      store.commit(Constant.FETCH_WALL, {wallList});
    }).catch(err => console.log(err));
  },
  [Constant.FETCH_FLOORING] : async (store) => {
    await axios.get(`${BASE_URL}/v3/materials`, {
      headers  : {
        'x-api-key': API_KEY
      }, params: {
        'include': {
          'material_products': {
            'join' : true,
            'where': {
              'm_plane_type_id': '2' // master code에 명시된 바닥재의 plane_type_id
            }
          },
        },
        'where'  : {
          'publish': true,
        },
        'link': 10
      }
    }).then(res => {
      const flooringList = [...res.data.data.materials];
      store.commit(Constant.FETCH_FLOORING, {flooringList});
    }).catch(err => console.log(err));
  },
  [Constant.GET_MASTER_CODE]: async (store) => {
    await axios.get(`${BASE_URL}/v3/master-codes`, {
      headers: {
        'x-api-key': API_KEY,
      },
      params : {
        'where': {
          'category': 'plane_type'
        }
      }
    }).then(res => {
      const masterCode = res.data;
      store.commit(Constant.GET_MASTER_CODE, {masterCode});
    }).catch(err => console.log(err));
  },
};
