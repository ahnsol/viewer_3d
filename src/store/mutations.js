import Constant from '../Constant';

export default {
  [Constant.FETCH_PRODUCTS] : (state, payload) => {
    state.productList = payload.productList.filter(product => product.thumbnail_path !== null).slice(0, 4);
  },
  [Constant.FETCH_WALL]     : (state, payload) => {
    state.wallList = payload.wallList.filter(wall => wall.thumbnail_path !== null).slice(0, 2);
  },
  [Constant.FETCH_FLOORING] : (state, payload) => {
    state.flooringList = payload.flooringList.filter(floor => floor.thumbnail_path !== null).slice(0, 2);
  },
  [Constant.GET_MASTER_CODE]: (state, payload) => {
    state.masterCode = payload.masterCode;
  }
};
