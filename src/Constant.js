export default {
  FETCH_PRODUCTS : 'fetchProducts',
  FETCH_WALL: 'fetchWall',
  FETCH_FLOORING: 'fetchFlooring',
  GET_MASTER_CODE: 'getMasterCode'
};
